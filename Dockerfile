
FROM node:16

ENV NODE_ENV development
ENV NPM_CONFIG_LOGLEVEL info

EXPOSE 3500

RUN npm install npm@7 -g

# Setup source directory
RUN mkdir /app
WORKDIR /app
COPY .npmrc .npmrc

COPY package.json package-lock.json /app/
RUN npm install -g typescript
RUN npm ci

# Copy app to source directory
COPY . /app

# Compile Typescript and Protobuffers
RUN npm run build

USER node
CMD ["npm", "start"]