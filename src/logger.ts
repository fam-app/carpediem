import pino from 'pino';

export default pino({
  prettyPrint: process.env.LOG_PRETTY_PRINT === 'true',
  messageKey: process.env.LOG_MESSAGE_KEY || 'msg',
  level: process.env.LOG_LEVEL || 'info',
  formatters: {
    level(label, number) {
      return {
        level: process.env.LOG_HUMAN_READABLE === 'false' ? number : label
      };
    }
  }
});
