import { Spanner, Database } from '@google-cloud/spanner';
import fp from 'fastify-plugin';

const PROJECT_ID = process.env.GCP_PROJECT_ID as string;
const INSTANCE_ID = process.env.GCP_SPANNER_INSTANCE_ID as string;
const DATABASE_ID = process.env.GCP_SPANNER_DATABASE_ID as string;

module.exports = fp(
  async function (fastify) {
    // Creates a Spanner client
    const spanner = new Spanner({
      projectId: PROJECT_ID
    });

    // Gets a reference to a Cloud Spanner instance and database
    const instance = spanner.instance(INSTANCE_ID);
    const database = instance.database(DATABASE_ID);

    fastify.decorate('spanner', database);
  },
  {
    fastify: '3.x',
    name: 'spanner-adapter'
  }
);

declare module 'fastify' {
  export interface FastifyInstance {
    spanner: Database;
  }
}
