import { TokenModel } from '../../models/token';
import fp from 'fastify-plugin';

module.exports = fp(
  async function (fastify) {
    fastify.decorate(
      'tokenModel',
      new TokenModel({
        spanner: fastify.spanner,
        logger: fastify.log
      })
    );
  },
  {
    fastify: '3.x',
    name: 'token-model',
    decorators: {
      fastify: ['spanner']
    },
    dependencies: ['spanner-adapter']
  }
);

declare module 'fastify' {
  export interface FastifyInstance {
    tokenModel: TokenModel;
  }
}
