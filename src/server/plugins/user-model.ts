import { UserModel } from '../../models/user';
import fp from 'fastify-plugin';

module.exports = fp(
  async function (fastify) {
    fastify.decorate(
      'userModel',
      new UserModel({
        spanner: fastify.spanner,
        logger: fastify.log
      })
    );
  },
  {
    fastify: '3.x',
    name: 'user-model',
    decorators: {
      fastify: ['spanner']
    },
    dependencies: ['spanner-adapter']
  }
);

declare module 'fastify' {
  export interface FastifyInstance {
    userModel: UserModel;
  }
}
