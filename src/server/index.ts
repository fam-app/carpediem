require('dotenv').config();

import autoload from 'fastify-autoload';
import { fastify } from 'fastify';
import { join } from 'path';
import logger from '../logger';

const DEFAULT_PORT = 3500;
const DEFAULT_HOST = '0.0.0.0';

const server = fastify({ logger });

server.register(autoload, {
  dir: join(__dirname, 'plugins'),
  options: {}
});
server.register(autoload, {
  dir: join(__dirname, 'routes'),
  options: {}
});

server.listen(
  process.env.PORT || DEFAULT_PORT,
  process.env.HOST || DEFAULT_HOST,
  function (err: Error) {
    if (err) {
      server.log.error(err, err.message);
      process.exit(1);
    }
  }
);
