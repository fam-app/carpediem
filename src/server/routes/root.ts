import { FastifyInstance, FastifyPluginOptions } from 'fastify';

module.exports = async function (
  fastify: FastifyInstance,
  opts: FastifyPluginOptions
) {
  fastify.get('/', async function (req, res) {
    res.send('Server is running');
  });
};
