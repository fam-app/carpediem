import { FastifyInstance, FastifyPluginOptions } from 'fastify';
import { Static, Type } from '@sinclair/typebox';

// ----------------------------------------------------------- //

const CreateUserParams = Type.Object({
  firstName: Type.String(),
  lastName: Type.String()
});
const User = Type.Object({
  id: Type.String(),
  firstName: Type.String(),
  lastName: Type.String()
});
const UserList = Type.Array(User);

type CreateUserParamsType = Static<typeof CreateUserParams>;

// ----------------------------------------------------------- //

module.exports = async function (
  fastify: FastifyInstance,
  opts: FastifyPluginOptions
) {
  fastify.log.debug('Loading user routes');

  fastify.route<{ Body: CreateUserParamsType }>({
    method: 'POST',
    url: '/users',
    schema: {
      body: CreateUserParams,
      response: {
        201: User
      }
    },
    handler: async (req, res) => {
      const {
        body: { firstName, lastName }
      } = req;

      req.log.debug('Creating new user');

      const id = await fastify.userModel.create({
        firstName,
        lastName
      });

      res.status(201).send({ id, firstName, lastName });
    }
  });

  fastify.route({
    method: 'GET',
    url: '/users',
    schema: {
      response: {
        200: UserList
      }
    },
    handler: async (req, res) => {
      req.log.debug('Fetching application users');

      const users = await fastify.userModel.list();
      res.send(users);
    }
  });
};
