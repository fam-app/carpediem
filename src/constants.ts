// Misc
export const TOKEN_LENGTH = 36;

// Tables
export const TOKENS_TABLE = 'tokens';
export const USERS_TABLE = 'users';
