import { FastifyLoggerInstance } from 'fastify';
import { v4 as uuid } from 'uuid';
import { Database, Table } from '@google-cloud/spanner';
import { USERS_TABLE } from '../constants';

export interface CreateUserParams {
  firstName: string;
  lastName: string;
}

export interface UserModelOptions {
  spanner: Database;
  logger: FastifyLoggerInstance;
}

interface UserModelTables {
  user: Table;
}

export class UserModel {
  private _spanner: Database;
  private _logger: FastifyLoggerInstance;
  private _tables: UserModelTables;

  /**
   * Create an instace of UserModel
   * @param spanner Spanner cluster database
   * @param logger Logger instance
   */
  constructor({ spanner, logger }: UserModelOptions) {
    this._spanner = spanner;
    this._logger = logger;

    this._tables = {
      user: this._spanner.table(USERS_TABLE)
    };
  }

  /**
   * Create a new user
   * @param firstName User first name
   * @param lastName User last name
   * @returns Promise representing the new user id
   */
  async create({ firstName, lastName }: CreateUserParams) {
    const id = uuid();
    this._logger.debug(`Creating new user:${id}`);

    await this._tables.user.insert({ id, firstName, lastName });
    return id;
  }

  /**
   * List all existing users
   * @returns Promise representing a list of users
   */
  async list() {
    // TODO: add pagination
    this._logger.debug('Fetching all database users');

    const [rows] = await this._tables.user.read({ keySet: { all: true } });
    return rows.map((row) => row.toJSON());
  }
}
