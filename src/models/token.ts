import { FastifyLoggerInstance } from 'fastify';
import randToken from 'rand-token';
import { v4 as uuid } from 'uuid';
import { Database, Table } from '@google-cloud/spanner';
import { TOKENS_TABLE, TOKEN_LENGTH } from '../constants';

export interface TokenModelOptions {
  spanner: Database;
  logger: FastifyLoggerInstance;
}

interface TokenModelTables {
  token: Table;
}

export class TokenModel {
  private _spanner: Database;
  private _logger: FastifyLoggerInstance;
  private _tables: TokenModelTables;

  /**
   * Create an instace of TokenModel
   * @param spanner Spanner cluster database
   * @param logger Logger instance
   */
  constructor({ spanner, logger }: TokenModelOptions) {
    this._spanner = spanner;
    this._logger = logger;

    this._tables = {
      token: this._spanner.table(TOKENS_TABLE)
    };
  }

  /**
   * Create a new token for the user
   * @param userId ID of the user that needs a new token
   * @returns Promise representing the operation result
   */
  async create(userId: string) {
    this._logger.debug(`Creating new token for user:${userId}`);

    const id = uuid();
    const val = randToken.generate(TOKEN_LENGTH);
    await this._tables.token.insert({ id, val, userId });
  }
}
